use strict;
use IO::File;
use File::Spec::Functions;
use File::Basename;
use File::Copy;
use File::Temp qw(tempfile);
use File::Remove qw(remove);
use File::FilterFuncs qw(filters $KEEP_LINE);
use File::Path::Tiny;
use YAML::Any qw(LoadFile);

sub readParams(@) {
    my @param = @_;
    my $pathin = $ARGV[0] or die "Error, requires the initial directory";
    (-d $pathin) or die "Error, directory '$pathin' could not be readen";
    my $pathout = "../src";
    return ($pathin,$pathout);
}

sub replace_include($_) {
    my $orig = $_[0];

    if (index($orig, "#include")<0) {
	return $orig;
    }

    # The R_ext references are not change
    if (index($orig, "R_ext")>0) {
       return $orig;
    }

    $orig =~ s#<.+/([^/]+)>#"$1"#;
    return $orig;
}

my ($pathin,$pathout)=readParams(@ARGV);

# Clear the directory
if (-d $pathout) {
    print "Removed $pathout\n";
    remove("$pathout/*");
}
else {
    File::Path::Tiny::mk("$pathout");
}

# Set the extensions to copy
my $ext = "{cc,h,cpp}";

# Get from the file the list of files to copy
## Copy all the additional files
print "Copied src-files\n";
my @files = glob("src-files/*");
my $config = LoadFile("depends.yaml");

for my $conf (@$config) {
    print "Copied Repository::$conf->{name}\n";
    push @files, grep {-f $_} map { glob(catfile($pathin, $conf->{directory}, "$_.$ext"));} @{$conf->{files}};
}

# Copy the files, changing the #include (there is no subdirectories) and avoid replacements
for my $file (@files) {
    my $dst = catfile($pathout, basename($file));

    if (-f $dst) {
       die "File '$dst' from '$file' was copied previously another directory";
    }
    filters($file, sub { $_ = replace_include $_; $KEEP_LINE}, $dst);
}

# Change the output
my $fndebug = catfile($pathout, "debug.cc");
if (-f $fndebug) {
   my ($foutput, $fnoutput) = tempfile();
   my $finput = new IO::File "$fndebug";
   print $foutput "\n#include <R_ext/Print.h>\n\n";

   # Add the lines of the debug changing vprintf with Rvprintf
   for (<$finput>) {
       s/\bvfprintf\(stderr,/REvprintf(/g;
       s/\bvprintf/Rvprintf/g;
       print $foutput $_;
   }

   $finput->close;
   $foutput->close;   

   # Rename the temporal file with the original name
   copy($fnoutput, $fndebug);
}
